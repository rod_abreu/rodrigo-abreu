/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhaseguradora.service;

import br.com.dbc.minhaseguradora.entity.Apolice;
import br.com.dbc.minhaseguradora.repository.ApoliceRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author rodrigo.abreu
 */
@Service
public class ApoliceService {
    
    @Autowired
    private ApoliceRepository apoliceRepository;
    
    //save ()
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Apolice save(Apolice apolice){
        apoliceRepository.save(apolice);
        return apolice;
    }
    
    //delete ( recebe um ID e deleta o ID)
    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void delete(Long id){
        apoliceRepository.deleteById(id);
    }
    //findById (recebe um Id, Long)
    public Optional<Apolice> findById(Long id){
        return apoliceRepository.findById(id);
    }
    
    //findAll (lista  todos)
    public Page<Apolice> findAll(Pageable pageable){
        return apoliceRepository.findAll(pageable);
    }
}
