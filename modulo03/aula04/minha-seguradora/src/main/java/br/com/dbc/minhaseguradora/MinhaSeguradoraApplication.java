package br.com.dbc.minhaseguradora;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class MinhaSeguradoraApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinhaSeguradoraApplication.class, args);
	}
}
