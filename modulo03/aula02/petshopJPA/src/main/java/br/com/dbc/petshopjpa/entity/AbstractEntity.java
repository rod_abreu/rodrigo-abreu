/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshopjpa.entity;

/**
 *
 * @author tiago.schmidt
 */
public abstract class AbstractEntity<I> {
    
    public abstract I getId();
    
}
