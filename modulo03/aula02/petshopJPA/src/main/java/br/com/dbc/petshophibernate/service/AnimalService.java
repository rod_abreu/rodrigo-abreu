/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshopjpa.dao.AnimalDAO;
import br.com.dbc.petshopjpa.entity.Animal;

/**
 *
 * @author tiago.schmidt
 */
public class AnimalService extends AbstractCrudService<Animal, Long, AnimalDAO>{
    
    private final AnimalDAO animalDAO;
    
    {
        animalDAO = new AnimalDAO();
    }
    
    @Override
    protected AnimalDAO getDAO(){
        return animalDAO;
    }
    
}
