-- CRIAR TABELA PAIS
CREATE TABLE PAIS(
    ID INTEGER NOT NULL PRIMARY KEY,
    NOME VARCHAR2(50) NOT NULL,
    SIGLA VARCHAR2(3) NOT NULL
);

CREATE SEQUENCE PAIS_SEQ;

-- CRIAR TABELA ESTADO
CREATE TABLE ESTADO(
    ID INTEGER NOT NULL PRIMARY KEY,
    NOME VARCHAR2(50) NOT NULL,
    SIGLA VARCHAR2(2) NOT NULL
);

CREATE SEQUENCE ESTADO_SEQ;

-- CRIAR TABELA CIDADE
CREATE TABLE CIDADE(
    ID INTEGER NOT NULL PRIMARY KEY,
    NOME VARCHAR2(50) NOT NULL,
    SIGLA VARCHAR2(2) NOT NULL
);

CREATE SEQUENCE CIDADE_SEQ;

-- CRIAR RELACIONAMENTO ESTADO X PAIS
CREATE TABLE ESTADO_PAIS(
    ID_ESTADO INTEGER NOT NULL REFERENCES ESTADO(ID), -- ESSA É UMA MANEIRA DE CRIAR A FOREIGN KEY (FK) ALÉM DO MODELO PADRÃO
    ID_PAIS INTEGER NOT NULL REFERENCES PAIS(ID)
);
-- CRIAR RELACIONAMENTO CIDADE X ESTADO
CREATE TABLE CIDADE_ESTADO(
    ID_CIDADE INTEGER NOT NULL REFERENCES CIDADE(ID),
    ID_ESTADO INTEGER NOT NULL REFERENCES ESTADO(ID)
);

-- COMMITAR AS INFOS
COMMIT;