/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.minhafloricultura.rest;

import br.com.dbc.DAO.VendaDAO;
import br.com.dbc.entity.Venda;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Path;

/**
 *
 * @author rodrigo.abreu
 */
@Stateless
@Path("venda")
public class VendaFacadeREST extends AbstractCrudREST<Venda, VendaDAO> {

    @Inject
    private VendaDAO vendaDAO;
    
    @Override
    protected VendaDAO getDAO(){
        return vendaDAO;
    }
}
