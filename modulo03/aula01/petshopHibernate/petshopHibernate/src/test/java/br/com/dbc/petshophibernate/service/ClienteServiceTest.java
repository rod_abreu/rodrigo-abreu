/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.service;

import br.com.dbc.petshophibernate.dao.ClienteDAO;
import br.com.dbc.petshophibernate.dao.HibernateUtil;
import br.com.dbc.petshophibernate.entity.Animal;
import br.com.dbc.petshophibernate.entity.Cliente;
import java.util.Arrays;
import java.util.List;
import javassist.NotFoundException;
import org.hibernate.Session;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 * @author tiago
 */
public class ClienteServiceTest {

    public ClienteServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCreate() {
        System.out.println("create");
        Cliente cliente1 = Cliente.builder()
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                                .nome("Animal 1")
                                .build()))
                .build();
        ClienteService.getInstance().create(cliente1);
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Cliente> clientes = session.createCriteria(Cliente.class).list();
        Assert.assertEquals("Quantidade de clientes errada", 1, clientes.size());
        Cliente result = clientes.stream().findAny().get();
        assertEquals("Cliente diferente", cliente1.getId(), result.getId());
        assertEquals("Quantidade animais diferente", 
                cliente1.getAnimalList().size(), 
                result.getAnimalList().size());
        assertEquals("Animais diferente", 
                cliente1.getAnimalList().stream().findAny().get().getId(), 
                result.getAnimalList().stream().findAny().get().getId());
        session.close();
    }
    @Test
    public void testCreateMocked() {
        System.out.println("Criar Cliente com Mock");
        
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(ArgumentMatchers.any());
        
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDao()).thenReturn(daoMock);
        
        Cliente cliente = Cliente.builder()
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                            .nome("Animal 1")
                            .build()))
                .build();
        clienteService.create(cliente);
        
        verify(daoMock, times(1)).createOrUpdate(any());
    }
    @Test
    public void testUpdateMocked() {
        System.out.println("Update Cliente com Mock");
        
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(ArgumentMatchers.any());
        
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDao()).thenReturn(daoMock);
        
        Cliente cliente = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                            .nome("Animal 1")
                            .build()))
                .build();
        clienteService.update(cliente);
        
        verify(daoMock, times(1)).createOrUpdate(any());
    }
    
    public void testDeleteMocked() throws NotFoundException {
        System.out.println("Delete Cliente com Mock");
        Cliente cliente = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                            .nome("Animal 1")
                            .build()))
                .build();
        
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(ArgumentMatchers.any());
        
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDao()).thenReturn(daoMock);
        
        Mockito.when(daoMock.findById(1l)).thenReturn(cliente);
        clienteService.delete(cliente.getId());
        
        verify(daoMock, times(1)).delete(cliente);
    }
    
    @Test
    public void testFindAllMocked(){
        Cliente cliente = Cliente.builder()
                .id(1l)
                .nome("Cliente 1")
                .animalList(
                        Arrays.asList(Animal.builder()
                            .nome("Animal 1")
                            .build()))
                .build();
        
        ClienteDAO daoMock = Mockito.mock(ClienteDAO.class);
        Mockito.doNothing().when(daoMock).createOrUpdate(ArgumentMatchers.any());
        
        ClienteService clienteService = Mockito.spy(ClienteService.class);
        Mockito.when(clienteService.getDao()).thenReturn(daoMock);
        
        List<Cliente> returned = clienteService.findAll();
        
        Mockito.verify(daoMock, times(1)).findAll);
        assertEquals(clienteList.size(), returned.size());
    }
    
    
    @Test(expected = IllegalArgumentException.class)
    public void testCreateException() {
        System.out.println("Criar Cliente com Mock");
        ClienteService.getInstance()
                .create(Cliente.builder()
                            .id(1l).build());
    }

}
