/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.petshophibernate.dao;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author rodrigo.abreu
 */
public abstract class AbstractDAO<ENTITY, ID> { 

    //generic nome deve ser maiúsculo
    private static final Logger LOG = Logger.getLogger(AbstractDAO.class.getName());
    
    protected abstract Class<ENTITY> getEntityClass();
    protected abstract String idProperty();
    
    public AbstractDAO () {
    }
    //procurando pelo Id
    public ENTITY findById(ID id){
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory()
            .openSession();
            return (ENTITY) session
                    .createCriteria(getEntityClass())
                    .add(Restrictions.eq("id", id))
                    .uniqueResult();
        }
        catch(Exception ex){
            LOG. log(Level.SEVERE, ex.getMessage(), ex);
            throw ex;
        }finally {
            if (session != null){
                session.close();
            }
        }
    }
    //create or update
    public void createOrUpdate(ENTITY entity){
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(entity);
            transaction.commit();      
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            if(session != null)
                session.close();
        }
    }
    
    public void delete(ENTITY entity) {
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            Transaction transaction = session.beginTransaction();
            session.delete(entity);
            transaction.commit();      
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            if(session != null)
                session.close();
        }
    }
    
    public List<ENTITY> findAll() {
        Session session = null;
        try{
            session = HibernateUtil.getSessionFactory().openSession();
            return session.createCriteria(getEntityClass()).list();
        }catch(Exception ex){
              throw ex;
        }finally {
            if (session != null){
                session.close();
            }
        }
    }
}
