INSERT INTO PRODUTO VALUES (PRODUTO_SEQ.NEXTVAL, 'VARA DE PESCA');

INSERT INTO PRODUTO VALUES (PRODUTO_SEQ.NEXTVAL, 'ANSOL DE PESCA');

UPDATE PRODUTO SET DESCRICAO = 'ANZOL DE PESCA' WHERE DESCRICAO = 'ANSOL DE PESCA';

ALTER TABLE PRODUTO
ADD QUANTIDADE NUMBER;

DELETE FROM PRODUTO WHERE DESCRICAO = 'ANZOL DE PESCA';

COMMIT;