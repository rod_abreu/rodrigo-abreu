/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.aula01;
import org.apache.commons.lang3.StringUtils;
/**
 *
 * @author rodrigoabreu
 */
public class Main {
    
    public static void main(String ... args) {
        
    
        System.out.println("isBlank/isEmpty");
        System.out.println("isBlank?"+StringUtils.isBlank("")); //StringUtils isBlank é true quando a String for s = ""
        System.out.println("'Rodrigo'isBlank?"+StringUtils.isBlank("Rodrigo")); //nesse caso ela é false
        System.out.println("isEmpty?"+StringUtils.isEmpty(" ")); // StringUtils isEmpty é true quando a String for s = " "
        
        System.out.println("leftPad");//insere dados a esquerda da String
        System.out.println(StringUtils.leftPad("rodrigo", 10)); //completa com espaços a esquerda da String até chegar a 10 caracteres
        System.out.println(StringUtils.leftPad("99788770", 9, '9')); //adiciona o 9 dígito
        
        System.out.println("rightPad");// insere dados a direita da String
        System.out.println(StringUtils.rightPad("rodrigo",10)); //completa com espaços a direita da String até 10 caracteres
        System.out.println(StringUtils.rightPad("9978877", 8, '0'));
        
        System.out.println("trimToNull"); //remove espaços na String
        System.out.println(StringUtils.trimToNull("  Rodrigo")); //remove espaços a esquerda
        System.out.println(StringUtils.trimToNull("Rodrigo  ")); //remove espaços a direita
        System.out.println(StringUtils.trimToNull("  Rodrigo  ")); //remove espaços a esq/dir
        
        System.out.println("Equals"); //compare two values, same value is true else false
        System.out.println(StringUtils.equals("ola", "ola")); //true
        System.out.println(StringUtils.equals("OLA", "ola")); //false
        System.out.println(StringUtils.equals("", "")); //true
        System.out.println(StringUtils.equals("", null)); //false
        
        System.out.println("Contains"); //verifica se contain na String determinado valor
        System.out.println(StringUtils.contains("ola","ola")); //ola contains ola -> true
        System.out.println(StringUtils.contains("ola","la"));  //ola contais la -> true
        System.out.println(StringUtils.contains(null, "")); //false
     }
}
