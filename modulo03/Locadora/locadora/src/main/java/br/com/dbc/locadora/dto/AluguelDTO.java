package br.com.dbc.locadora.dto;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author rodrigo.abreu
 */
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class AluguelDTO implements Serializable{
    
    private Long idCliente;
    private List<Long> midias;
    
}
