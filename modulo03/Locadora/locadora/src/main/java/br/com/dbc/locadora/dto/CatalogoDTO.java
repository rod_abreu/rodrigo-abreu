package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author rodrigo.abreu
 */
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class CatalogoDTO implements Serializable{
    
    private Filme filme;
    private List<Midia> midia;
    private HashMap<MidiaType, Double> midiaValores;
    
}
