package br.com.dbc.locadora.entity;


/**
 *
 * @author rodrigo.abreu
 * @param <ID>
 */

public abstract class AbstractEntity<ID> {
    
    public abstract ID getId();
}