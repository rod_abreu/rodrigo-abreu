/*
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author rodrigo.abreu
 */
public interface MidiaRepository extends JpaRepository<Midia, Long> {
    
    public long countByTipo(MidiaType tipo);
    
    public List<Midia> findByIdFilmeIdAndIdAluguelIsNull(Long id);

    
    public long countByTipoAndIdFilme(MidiaType tipo, Filme idFilme);

    public Page<Midia> findByIdFilmeTituloContainingIgnoreCaseOrIdFilmeCategoriaOrIdFilmeLancamentoBetween(String titulo, Categoria categoria, 
            LocalDate lancamentoIni, LocalDate lancamentoIniFim, Pageable pageable);   

    @Query("select m from Midia m where m.idFilme.id =:idFilme and m.tipo =:tipo")
    public List<Midia> findByFilmeAndMidiaTipo(@Param("idFilme")Long id, @Param("tipo")MidiaType tipo);

}