package br.com.dbc.locadora.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author rodrigo.abreu
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Cliente extends AbstractEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @NotNull
    private Long id;
    
    @NotNull
    @Column(name = "NOME")
    @Size(min = 2, max = 100)
    private String nome;
    
    @NotNull
    @Size(min = 5, max = 100)
    @Column(name = "TELEFONE")
    private String telefone;
    
    @NotNull
    @Size(min = 5, max = 50)
    @Column(name = "RUA")
    private String rua;
    
//    
//    @Size(min = 5, max = 50)
//    @Column(name = "NUMERO")
//    private Long numero;
    
    @NotNull
    @Size(min = 5, max = 50)
    @Column(name = "BAIRRO")
    private String bairro;
    
    @NotNull
    @Size(min = 5, max = 50)
    @Column(name = "CIDADE")
    private String cidade;
    
    @NotNull
    @Size(min = 5, max = 50)
    @Column(name = "ESTADO")
    private String estado;
    
   
}
