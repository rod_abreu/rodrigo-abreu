package br.com.dbc.locadora.service;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.repository.MidiaRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MidiaService extends AbstractCrudService<Midia> {

    @Autowired
    private MidiaRepository midiaRepository;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected JpaRepository<Midia, Long> getRepository() {
        return midiaRepository;
    }

    public long countByTipo(MidiaType tipo) {
        return midiaRepository.countByTipo(tipo);
    }

    public long countByTipoAndIdFilme(MidiaType tipo, Filme idFilme) {
        return midiaRepository.countByTipoAndIdFilme(tipo, idFilme);
    }

    public List<Midia> findAllByIdFilmeAndType(Long id, MidiaType midiaType) {
        ArrayList<Midia> midiasAchadas = new ArrayList<>();
        List<Midia> midias = getRepository().findAll();
        for (Midia m : midias) {
            if (m.getIdFilme().getId() == id && m.getTipo() == midiaType) {
                midiasAchadas.add(m);
            }
        }
        return midiasAchadas;
    }
    
    //retorna uma lista de midia com o tipo de midia e valores de cada midia
    public List<Midia> findByIdFilmeId(Long id){
        return midiaRepository.findByIdFilmeIdAndIdAluguelIsNull(id);
    }

    public List<Midia> findAllByIdFilme(Long id) {
        ArrayList<Midia> midiasAchadas = new ArrayList<>();
        List<Midia> midias = getRepository().findAll();
        for (Midia m : midias) {
            if (m.getIdFilme().getId() == id) {
                midiasAchadas.add(m);
            }
        }
        return midiasAchadas;
    }

    @Transactional(readOnly = false)
    void salvarComFilme(Filme filme, List<MidiaDTO> midiaDTOList) {
        midiaDTOList.forEach(midiaDTO -> {
            for (int i = 0; i < midiaDTO.getQuantidade(); i++) {
                Midia m = new Midia();
                m.setTipo(midiaDTO.getTipo());
                m.setIdFilme(filme);
                m = midiaRepository.save(m);
                valorMidiaService.salvarComMidia(m, midiaDTO.getValor());
            }
        });

    }

    @Transactional(readOnly = false)
    void atualizarComFilme(Filme filme, List<MidiaDTO> midiaDTOList) {
        midiaDTOList.forEach(midiaDTO -> {
            List<Midia> midiasJahExistentes = midiaRepository.findByFilmeAndMidiaTipo(filme.getId(), midiaDTO.getTipo());
            int sizeAtual = midiasJahExistentes.size();
            if (midiaDTO.getQuantidade() > sizeAtual) {
                int diferenca = midiaDTO.getQuantidade() - sizeAtual;
                for (int i = 0; i < diferenca; i++) {
                    Midia m = new Midia();
                    m.setTipo(midiaDTO.getTipo());
                    m.setIdFilme(filme);
                    m = midiaRepository.save(m);
                    valorMidiaService.salvarComMidia(m, midiaDTO.getValor());
                }
            } else if (midiaDTO.getQuantidade() < sizeAtual) {
                int diferenca = sizeAtual - midiaDTO.getQuantidade();
                Stack<Midia> midiasNaoAlugadas = new Stack<>();
                for (Midia midiasJahExistente : midiasJahExistentes) {
                    if (midiasJahExistente.getIdAluguel() == null) {
                        midiasNaoAlugadas.push(midiasJahExistente);
                    }
                }
                for (int i = 0; i < diferenca; i++) {
                    Midia midiaDeletar = midiasNaoAlugadas.pop();
                    valorMidiaService.deleteByIdMidia(midiaDeletar);
                    midiaRepository.deleteById(midiaDeletar.getId());
                }
            }
            midiasJahExistentes = midiaRepository.findByFilmeAndMidiaTipo(filme.getId(), midiaDTO.getTipo());
            valorMidiaService.atualizarComMidia(midiasJahExistentes, midiaDTO.getValor());

        });
    }

}
