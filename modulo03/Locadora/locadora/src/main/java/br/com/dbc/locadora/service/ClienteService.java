/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.service;

import br.com.dbc.locadora.config.SoapConnector;
import br.com.dbc.locadora.dto.CepDTO;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.repository.ClienteRepository;
import br.com.dbc.locadora.ws.ConsultaCEP;
import br.com.dbc.locadora.ws.ConsultaCEPResponse;
import br.com.dbc.locadora.ws.ObjectFactory;
import javax.xml.bind.JAXBElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author rodrigo.abreu
 */
@Service
public class ClienteService extends AbstractCrudService<Cliente>{
    
    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    protected JpaRepository<Cliente, Long> getRepository() {
        return clienteRepository;
    }
    
    @Autowired SoapConnector soapConnector;
    @Autowired ObjectFactory objectFactory;
    
    public CepDTO consultaCep(String cep){
        
        CepDTO cepDto = new CepDTO();
        
        ConsultaCEP consulta = objectFactory.createConsultaCEP();
        consulta.setCep(cep);
        ConsultaCEPResponse response = ((JAXBElement<ConsultaCEPResponse>)soapConnector
                .callWebService(
                "https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente", 
                        objectFactory.createConsultaCEP(consulta))).getValue();
        response.getReturn().getEnd();
        
        cepDto.setRua(response.getReturn().getEnd());
        cepDto.setBairro(response.getReturn().getBairro());
        cepDto.setCidade(response.getReturn().getCidade());
        cepDto.setEstado(response.getReturn().getUf());
        
        return cepDto;
    }
}
