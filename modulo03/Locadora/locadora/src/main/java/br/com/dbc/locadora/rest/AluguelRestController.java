package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.service.AluguelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/aluguel")
public class AluguelRestController extends AbstractRestController<Aluguel, AluguelService>{

    @Autowired
    private AluguelService aluguelService;

    @Override
    protected AluguelService getService() {
        return aluguelService;
    }
    @PostMapping("/retirada")
   @Transactional(readOnly = false, rollbackFor = Exception.class)
   public ResponseEntity<?> post(@RequestBody AluguelDTO input) {
       return ResponseEntity.ok(getService().retirada(input));
   }
   
   @PostMapping("/devolucao")
   @Transactional(readOnly = false, rollbackFor = Exception.class)
   public ResponseEntity<?> devolucao(@RequestBody AluguelDTO input) {
       return ResponseEntity.ok(getService().devolucao(input.getMidias()));
   }
   
   @GetMapping("/devolucao")
   @Transactional(readOnly = false, rollbackFor = Exception.class)
   public ResponseEntity<?> devolucao(@RequestBody Pageable pageable) {
       return ResponseEntity.ok(getService().devolucaoHoje(pageable));
   }
}