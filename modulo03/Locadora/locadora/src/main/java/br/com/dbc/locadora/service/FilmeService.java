package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.CatalogoDTO;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.FilmeRepository;
import br.com.dbc.locadora.repository.MidiaRepository;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import jdk.nashorn.internal.objects.NativeArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author rodrigo.abreu
 */
@Service
public class FilmeService extends AbstractCrudService<Filme> {

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Autowired
    private FilmeRepository filmeRepository;

    @Autowired
    private MidiaRepository midiaRepository;

    @Override
    protected JpaRepository<Filme, Long> getRepository() {
        return filmeRepository;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme salvarComMidia(@RequestBody FilmeDTO dto) {
        
        Filme filme = new Filme();
        filme.setCategoria(dto.getCategoria());
        filme.setLancamento(dto.getLancamento());
        filme.setTitulo(dto.getTitulo());
        filme = filmeRepository.save(filme);
        
        midiaService.salvarComFilme(filme, dto.getMidia());
        
        return filme;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public Filme atualizarComMidia(@NotNull @Valid FilmeDTO dto) {

        Filme filme = filmeRepository.findById(dto.getId()).get();
        filme.setCategoria(dto.getCategoria());
        filme.setLancamento(dto.getLancamento());
        filme.setTitulo(dto.getTitulo());
        filme = filmeRepository.save(filme);
        
        midiaService.atualizarComFilme(filme, dto.getMidia());
        
        return filme;
    }

    public Page<Filme> search(
            Pageable pageable,
            Categoria categoria,
            String titulo,
            LocalDate inicio,
            LocalDate fim) {
        inicio = inicio == null ? LocalDate.MIN : inicio;
        fim = fim == null ? LocalDate.MIN : fim;
        return filmeRepository
                .findByTituloContainingIgnoreCaseOrCategoriaOrLancamentoBetween(
                        pageable,
                        titulo,
                        categoria,
                        inicio,
                        fim
                );
    }

    public int countByTipo(Long id, MidiaType midiaType) {
        List<Midia> midias = midiaService.findAllByIdFilmeAndType(id, midiaType);
        return midias.size();
    }

    public List<ValorMidia> findPrecoFilme(Long id) {
        List<Midia> midias = midiaService.getRepository().findAll();
        List<ValorMidia> vms = valorMidiaService.getRepository().findAll();

        List<ValorMidia> vmsEncontradas = new ArrayList<>();

        for (Midia m : midias) {
            if (m.getIdFilme().getId() == id) {
                for (ValorMidia vm : vms) {
                    if (vm.getIdMidia() == m) {
                        vmsEncontradas.add(vm);
                    }
                }
            }
        }
        return vmsEncontradas;
    }

    public List<CatalogoDTO> consultaCatalogo(Pageable pageable, String titulo) {

        //criar uma lista de filmes com base na String passada
        //verifica filmes disponíveis, caso filme indisponível, retorna a data mais próxima de devolução do filme
        //se disponível, lista as mídias com o valor de aluguel para cada
        
        //cria um List para ser o catálogo
        List<CatalogoDTO> catalogoList = new ArrayList<>();

        //cria um List dos filmes com base na string informada pelo título
        List<Filme> filmes = (List<Filme>) filmeRepository.findByTituloContainingIgnoreCase(titulo);
        
        //para cada filme, cria uma lista de midias
        for(Filme filme : filmes){
            List<Midia> midias = midiaService.findByIdFilmeId(filme.getId());
            
            HashMap<MidiaType, Double> midiaValor = new HashMap<>();
            
            for(Midia midia : midias){
                ValorMidia valorMidia = valorMidiaService.findByValoresMidia(midia.getId());
                midiaValor.put(midia.getTipo(),valorMidia.getValor() );
                
                CatalogoDTO catalogo = CatalogoDTO.builder()
                        .filme(filme)
                        .midia(midias)
                        .midiaValores(midiaValor).build();
                
                catalogoList.add(catalogo);
            }
            

        }
        return catalogoList;
    }
}
