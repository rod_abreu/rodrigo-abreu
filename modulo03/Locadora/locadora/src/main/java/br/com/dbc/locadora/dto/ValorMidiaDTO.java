/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.MidiaType;
import lombok.Data;

/**
 *
 * @author rodrigo.abreu
 */

@Data
public class ValorMidiaDTO {
    
    private MidiaType tipo;
    
    private Integer quantidade;
    
    private Double valor;
    
    
}
