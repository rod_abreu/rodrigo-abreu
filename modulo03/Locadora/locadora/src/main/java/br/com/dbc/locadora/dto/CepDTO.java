package br.com.dbc.locadora.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author rodrigo.abreu
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CepDTO {
    
    private String rua;
    private String bairro;
    private String cidade;
    private String estado;
   
}
