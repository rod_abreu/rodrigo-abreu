package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.MidiaDTO;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.repository.ValorMidiaRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author rodrigo.abreu
 */
@Service
public class ValorMidiaService extends AbstractCrudService<ValorMidia> {

    @Autowired
    private ValorMidiaRepository valorMidiaRepository;

    @Override
    protected JpaRepository<ValorMidia, Long> getRepository() {
        return valorMidiaRepository;
    }

    public double findAllValorMidias(List<Long> idMidias) {
        double soma = 0;
        List<ValorMidia> valorMidias = getRepository().findAll();
        for (ValorMidia vm : valorMidias) {
            for (Long i : idMidias) {
                //Verifica se a midia do valorMidia possui o mesmo ID de uma das midias que foram passadas
                //por parametro
                if (vm.getIdMidia().getId() == i) {
                    soma += vm.getValor();
                }
            }
        }
        return soma;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public ValorMidia updateFimVigencia(Midia midia) {
        ValorMidia valorMidiaAntigo = findByIdMidia(midia.getId());
        ValorMidia valorMidiaAtualizada = ValorMidia.builder()
                .idMidia(midia)
                .valor(valorMidiaAntigo.getValor())
                .inicioVigencia(valorMidiaAntigo.getInicioVigencia())
                .finalVigencia(LocalDateTime.now()).build();
        return getRepository().save(valorMidiaAtualizada);
    }

    public ValorMidia findByIdMidia(Long id) {
        List<ValorMidia> valorMidias = getRepository().findAll();
        for (ValorMidia vm : valorMidias) {
            if (vm.getIdMidia().getId() == id) {
                return vm;
            }
        }
        return null;
    }


    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void updateValorMidia(List<Midia> lista, double valor) {
        for (Midia m : lista) {
            ValorMidia valorMidiaAntiga = findByIdMidia(m.getId());
            ValorMidia novoValorMidia = ValorMidia.builder()
                    .idMidia(m)
                    .valor(valor)
                    .inicioVigencia(valorMidiaAntiga.getInicioVigencia())
                    .finalVigencia(LocalDateTime.now()).build();
            getRepository().save(novoValorMidia);
        }
    }

    public List<ValorMidia> findAllByIdFilme(Long id) {
        List<ValorMidia> valorMidias = getRepository().findAll();
        ArrayList<ValorMidia> vms = new ArrayList<>();
        for (ValorMidia vm : valorMidias) {
            if (vm.getIdMidia().getIdFilme().getId() == id) {
                vms.add(vm);
            }
        }
        return vms;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public void savePreco(List<Midia> lista, Double valor) {
        for (Midia m : lista) {
            getRepository().save(ValorMidia.builder()
                    .idMidia(m)
                    .valor(valor)
                    .inicioVigencia(LocalDateTime.now())
                    .build());
        }
    }
    @Transactional(readOnly = false)
    public void deleteByIdMidia(Midia midia) {
        valorMidiaRepository.deleteByMidiaId(midia.getId());
    }

    @Transactional(readOnly = false)
    void salvarComMidia(Midia midia, Double valor) {
        ValorMidia vm = new ValorMidia();
        vm.setValor(valor);
        vm.setIdMidia(midia);
        vm.setInicioVigencia(LocalDateTime.now());
        vm.setFinalVigencia(null);
        valorMidiaRepository.save(vm);
    }

    @Transactional(readOnly = false)
    void atualizarComMidia(List<Midia> midiasJahExistentes, Double valor) {
        midiasJahExistentes.forEach(midia->{
            ValorMidia valorVigente = valorMidiaRepository.findByMidiaIdAndVigente(midia.getId());
            if(!valorVigente.getValor().equals(valor)){
                valorVigente.setFinalVigencia(LocalDateTime.now());
                valorMidiaRepository.save(valorVigente);
                valorVigente = new ValorMidia();
                valorVigente.setValor(valor);
                valorVigente.setIdMidia(midia);
                valorVigente.setInicioVigencia(LocalDateTime.now());
                valorVigente.setFinalVigencia(null);
                valorMidiaRepository.save(valorVigente);
            }
        });
    }
    
    public ValorMidia findByValoresMidia(Long id){
        return valorMidiaRepository.findByMidiaIdAndVigente(id);
    }

}
