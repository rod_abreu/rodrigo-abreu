package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.Aluguel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author rodrigo.abreu
 */
public interface AluguelRepository extends JpaRepository<Aluguel, Long> {
    
}
