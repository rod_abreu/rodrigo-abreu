package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.ValorMidia;
import br.com.dbc.locadora.service.ValorMidiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/valor-midia")
public class ValorMidiaRestController extends AbstractRestController<ValorMidia, ValorMidiaService>{

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected ValorMidiaService getService() {
        return valorMidiaService;
    }
}