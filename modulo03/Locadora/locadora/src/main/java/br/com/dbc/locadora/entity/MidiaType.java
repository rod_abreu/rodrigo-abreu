/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.entity;

/**
 *
 * @author rodrigo.abreu
 */
public enum MidiaType {
    DVD, VHS, BLUE_RAY
}
