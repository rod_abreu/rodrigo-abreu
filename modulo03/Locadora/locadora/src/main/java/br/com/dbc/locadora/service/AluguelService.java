package br.com.dbc.locadora.service;

import br.com.dbc.locadora.dto.AluguelDTO;
import br.com.dbc.locadora.dto.AluguelDTOValor;
import br.com.dbc.locadora.entity.Aluguel;
import br.com.dbc.locadora.entity.Cliente;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.repository.AluguelRepository;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Period;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author rodrigo.abreu
 */
@Service
public class AluguelService extends AbstractCrudService<Aluguel> {

    @Autowired
    private AluguelRepository aluguelRepository;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private MidiaService midiaService;

    @Autowired
    private ValorMidiaService valorMidiaService;

    @Override
    protected JpaRepository<Aluguel, Long> getRepository() {
        return aluguelRepository;
    }

    @Transactional(readOnly = false, rollbackFor = Exception.class)
    public AluguelDTOValor retirada(AluguelDTO aluguelDTO) {
        
        Aluguel aluguel = Aluguel.builder()
                .retirada(LocalDate.now())
                .idCliente((Cliente) clienteService.findById(aluguelDTO.getIdCliente()).get())
                .previsao(LocalDate.now().plusDays(aluguelDTO.getMidias().size()))
                .build();

        List<Midia> midias = new ArrayList<Midia>();

        getRepository().save(aluguel);

        for (int i = 0; i < aluguelDTO.getMidias().size(); i++) {

            Midia midia = (Midia) midiaService.findById(aluguelDTO.getMidias().get(i)).get();

            midia.setIdAluguel(aluguel);

            midias.add(midia);

            midiaService.save(midia);
        }

        aluguel.setMidiaList(midias);

        aluguel.setPrevisao(LocalDate.now().plusDays(midias.size()));

        List<Long> idMidias = new ArrayList<>();
        for (int i = 0; i < aluguel.getMidiaList().size(); i++) {
            idMidias.add(aluguel.getMidiaList().get(i).getId());
        }
        double valorTotalMidias = valorMidiaService.findAllValorMidias(idMidias);
        BigDecimal valorAluguel = new BigDecimal(valorTotalMidias).setScale(2, RoundingMode.HALF_EVEN);
        AluguelDTOValor aluguelComValor = AluguelDTOValor.builder()
                .id(aluguel.getId())
                .retirada(aluguel.getRetirada())
                .previsao(aluguel.getPrevisao())
                .devolucao(aluguel.getDevolucao())
                .valor(valorAluguel)
                .idCliente(aluguel.getIdCliente())
                .build();
        getRepository().save(aluguel);
        return aluguelComValor;
    }

    @Transactional(readOnly = false)
    public AluguelDTOValor devolucao(List<Long> idMidias) {

        Midia primeiraMidia = (Midia) midiaService.findById(idMidias.get(0)).get();

        Long idAluguel = primeiraMidia.getIdAluguel().getId();

        Aluguel aluguel = (Aluguel) getRepository().findById(idAluguel).get();

        List<Midia> midias = aluguel.getMidiaList();

        List<Midia> midiasRemove = new ArrayList<>();

        for (Long idMidia : idMidias) {

            for (Midia midia : midias) {

                if (midia.getId().equals(idMidia)) {
                    midiasRemove.add(midia);
                }
            }

            Midia midia = (Midia) midiaService.findById(idMidia).get();
            midia.setIdAluguel(null);
            midiaService.save(midia);
        }

        double valorTotalMidias = valorMidiaService.findAllValorMidias(idMidias);

        midias.removeAll(midiasRemove);

        aluguel.setMidiaList(midias);
        double multa = 0;

        if (midias.isEmpty()) {

            aluguel.setDevolucao(ZonedDateTime.now());

            if (ZonedDateTime.now().toLocalDate().equals(aluguel.getPrevisao()) && ZonedDateTime.now().getHour() > 16) {
                multa = 1;

            } else if (ZonedDateTime.now().toLocalDate().isAfter(aluguel.getPrevisao())) {

                multa = Period.between(ZonedDateTime.now().toLocalDate(), aluguel.getPrevisao()).getDays();
            }
        }

        BigDecimal valorSemMulta = new BigDecimal(valorTotalMidias).setScale(2, RoundingMode.HALF_EVEN);
        BigDecimal valorAluguel = new BigDecimal(multa * valorTotalMidias).setScale(2, RoundingMode.HALF_EVEN);
        aluguel.setMulta(valorAluguel);
        getRepository().save(aluguel);
        AluguelDTOValor aluguelComValor = AluguelDTOValor.builder()
                .id(aluguel.getId())
                .retirada(aluguel.getRetirada())
                .previsao(aluguel.getPrevisao())
                .devolucao(aluguel.getDevolucao())
                .valor(valorSemMulta)
                .multa(valorAluguel)
                .total(valorAluguel.add(valorSemMulta).setScale(2, RoundingMode.HALF_EVEN))
                .idCliente(aluguel.getIdCliente())
                .build();
        return aluguelComValor;
    }

    public List<Aluguel> findByPrevisao(LocalDate previsao) {
        List<Aluguel> todosAlugueis = getRepository().findAll();
        ArrayList<Aluguel> alugueisHoje = new ArrayList<>();

        for (Aluguel a : todosAlugueis) {
            if (a.getPrevisao() == previsao) {
                alugueisHoje.add(a);
            }
        }
        return alugueisHoje;
    }

    public Page<Filme> devolucaoHoje(Pageable pageable) {
        List<Aluguel> alugueisHoje = findByPrevisao(LocalDate.now());
        ArrayList<Filme> filmes = new ArrayList<>();
        for (Aluguel a : alugueisHoje) {
            for (Midia m : a.getMidiaList()) {
                filmes.add(m.getIdFilme());
            }
        }
        return new PageImpl<>(filmes, pageable, filmes.size());
    }
}
