package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.dto.CatalogoSearchDTO;
import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.dto.FilmeDTO;
import br.com.dbc.locadora.entity.Categoria;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.service.FilmeService;
import br.com.dbc.locadora.service.MidiaService;
import java.time.LocalDate;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/filme")
public class FilmeRestController extends AbstractRestController<Filme, FilmeService>{

    @Autowired
    private FilmeService filmeService;
    
    @Autowired
    private MidiaService midiaService;
    
    @Override
    protected FilmeService getService() {
        return filmeService;
    }
    
    @PutMapping("/{id}/midia")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody FilmeDTO filmeDTO) {
        if (id == null || !Objects.equals(filmeDTO.getId(), id)) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(filmeService.atualizarComMidia(filmeDTO));
    }
   @GetMapping("/search")
    public ResponseEntity<Page<Filme>> get(
            Pageable pageable,
            @RequestParam(value = "categoria", required = false) Categoria categoria, 
            @RequestParam(value = "titulo", defaultValue = "", required = false) String titulo, 
            @RequestParam(value = "lancamentoIni", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate inicio,
            @RequestParam(value = "lancamentoFim", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate fim){
        return ResponseEntity.ok(filmeService.search(pageable, categoria, titulo, inicio, fim));
    }
    
    @GetMapping("count/{id}/{midiaType}")
    public ResponseEntity<?> countByTipo(@PathVariable Long id, @PathVariable MidiaType midiaType) {
        return ResponseEntity.ok(getService().countByTipo(id, midiaType));
    }
     
    @GetMapping("precos/{id}")
    public ResponseEntity<?> findPrecoFilme(@PathVariable Long id) {
        return ResponseEntity.ok(getService().findPrecoFilme(id));
    }
    
    @PostMapping("/midia")
    public ResponseEntity<?> salvarComMidia(@RequestBody FilmeDTO dto) {
        Filme filme = filmeService.salvarComMidia(dto);
        return ResponseEntity.ok(filme);
    }
    
   @PostMapping("/search/catalogo")
   public ResponseEntity<?> consultaCatalogo(Pageable pageable, @RequestBody CatalogoSearchDTO dto){
       return ResponseEntity.ok(getService().consultaCatalogo(pageable, dto.getTitulo()));
   }
}