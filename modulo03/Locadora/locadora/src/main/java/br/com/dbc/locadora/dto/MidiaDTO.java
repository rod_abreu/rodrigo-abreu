package br.com.dbc.locadora.dto;

import br.com.dbc.locadora.entity.Filme;
import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import lombok.Getter;

/**
 *
 * @author rodrigo.abreu
 */
@Getter
public class MidiaDTO {

    private MidiaType tipo;

    private Integer quantidade;

    private Double valor;

}
