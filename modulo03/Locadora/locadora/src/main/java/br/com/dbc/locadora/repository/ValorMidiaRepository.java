/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.dbc.locadora.repository;

import br.com.dbc.locadora.entity.ValorMidia;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author rodrigo.abreu
 */
public interface ValorMidiaRepository extends JpaRepository<ValorMidia, Long> {

    @Modifying
    @Query("delete from ValorMidia vm where vm.idMidia.id =:idMidia")
    public void deleteByMidiaId(@Param("idMidia")Long idMidia);

    @Query("select vm from ValorMidia vm where vm.idMidia.id = :idMidia and vm.finalVigencia is null")
    public ValorMidia findByMidiaIdAndVigente(@Param("idMidia")Long idMidia);
    
    
        
}
