package br.com.dbc.locadora.rest;

import br.com.dbc.locadora.entity.Midia;
import br.com.dbc.locadora.entity.MidiaType;
import br.com.dbc.locadora.service.MidiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/midia")
public class MidiaRestController extends AbstractRestController<Midia, MidiaService>{

    @Autowired
    private MidiaService midiaService;

    @Override
    protected MidiaService getService() {
        return midiaService;
    }
    
    @GetMapping("/count/{tipo}")
    public ResponseEntity<Long> get(@PathVariable MidiaType tipo){
        return ResponseEntity.ok(midiaService.countByTipo(tipo));
    }
}