describe( 'formatar elfos', function() {
    beforeEach( function() {
        chai.should()
      } )

      it('um objeto com exp', function () {
        const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 6 }
        const elfo2 = { nome: "Galadriel", experiencia: 1, qtdFlechas: 1 }
        const arrayElfos = [ elfo1, elfo2 ]

        const esperado = [{
            nome: "LEGOLAS",
            qtdFlechas: 6,
            temExperiencia: false,
            descricao: "LEGOLAS sem experiência e com 6 flechas"
        },
        {
            nome: "GALADRIEL",
            qtdFlechas: 1,
            temExperiencia: true,
            descricao: "GALADRIEL com experiência e com 1 flecha"
        }]

        const resultado = formatarElfos(arrayElfos)
        resultado.should.eql(esperado)
    })

    it('deveFormatarOsObjetosElfos', function () {
        const elfo1 = { nome: "Legolas", experiencia: 0, qtdFlechas: 6 }
        const elfo2 = { nome: "Galadriel", experiencia: 1, qtdFlechas: 1 }
        const arrayElfos = [ elfo1, elfo2 ]

        const esperado = [{
            nome: "LEGOLAS",
            qtdFlechas: 6,
            temExperiencia: false,
            descricao: "LEGOLAS sem experiência e com 6 flechas"
        },
        {
            nome: "GALADRIEL",
            qtdFlechas: 1,
            temExperiencia: true,
            descricao: "GALADRIEL com experiência e com 1 flecha"
        }]

        const resultado = formatarElfos(arrayElfos)
        resultado.should.eql(esperado)
    })
})