function rodarPrograma(){
    
    const h1 = document.getElementById("horario")
    const btnReiniciar = document.getElementById("btnReiniciar")
    const btnPararRelogio = document.getElementById("btnPararRelogio")  
    const segundos = 1000 
    
    var atualizarH1 = function (){
        h1.innerText = new Date().toLocaleTimeString()
    }
    
    btnReiniciar.disabled = true
    
    atualizarH1()
    var idInterval = setInterval(atualizarH1,segundos)  

    const relogio = new Relogio()
}
rodarPrograma()