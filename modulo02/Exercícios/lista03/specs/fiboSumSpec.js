describe( 'fiboSum', function() {

    beforeEach( function() {
        chai.should()
      } )

      it( 'A soma dos 7 primeiros números  = 33', function() {
        const num = 7
        const resultado = fiboSum(num)
        resultado.should.equal(33)
      } )

      it( 'A soma dos 1 primeiros números  = 1', function() {
        const num = 1
        const resultado = fiboSum(num)
        resultado.should.equal(1)
      } )

      it( 'A soma dos 0 primeiros números  = 0', function() {
        const num = 0
        const resultado = fiboSum(num)
        resultado.should.equal(0)
      } )

      it( 'A soma dos -1 primeiros números  = 0', function() {
        const num = -1
        const resultado = fiboSum(num)
        resultado.should.equal(0)
      } )
    } )