describe( 'concatenarSemNull', function() {

    beforeEach( function() {
        chai.should()
      } )

      it( 'concatenar quando for null e Soja', function() {
        const x = null
        const y = "Soja"
        const resultado = concatenarSemNull(x,y)
        resultado.should.equal("Soja")
      } )

      it( 'concatenar quando for os parâmtros -> Soja e é bom', function() {
        const x = "Soja "
        const y = "é bom"
        const resultado = concatenarSemNull(x,y)
        resultado.should.equal("Soja é bom")
      } )

      it( 'concatenar Soja + undefined -> Soja éundefined', function() {
        const x = "Soja é"
        const resultado = concatenarSemNull(x)
        resultado.should.equal("Soja é")
      } )

    } )