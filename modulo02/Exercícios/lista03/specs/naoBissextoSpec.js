describe( 'naoBissexto', function() {

    beforeEach( function() {
        chai.should()
      } )

      it( 'verifica se 2016 não é Bissexto', function() {
        const ano = 2016
        const resultado = naoBissexto(ano)
        resultado.should.equal(false)
      } )

      it( 'verifica se 2017 não é Bissexto', function() {
        const ano = 2017
        const resultado = naoBissexto(ano)
        resultado.should.equal(true)
      } )

      it( 'verifica se  1900 não é Bissexto', function() {
        const ano = 1900
        const resultado = naoBissexto(ano)
        resultado.should.equal(true)
      } )

      it( 'verifica se  2008 não é Bissexto', function() {
        const ano = 2008
        const resultado = naoBissexto(ano)
        resultado.should.equal(false)
      } )
})