function main(){

    const h1 = document.getElementsByClassName('contador')
    var contador = 30
    
    h1.innerText = contador  //passando o valor inicial do contador (30 seg)

    const idInterval = setInterval(function(){
        var value = parseInt(h1.innerText) //transforma o valor em int
        
        if(value !== 0){ //atualiza valor de counter
            console.log(value)
            value-- 
            h1.innerText = value
            
        }else{
            clearInterval(idInterval) //
            h1.innerText = 'Timeout!!!' 
        }
    },1000)
}

main()