describe( 'somar posições pares', function() {
    beforeEach( function() {
        chai.should()
      } )

      it( 'soma valores das posições pares do array aleatório', function() {
        const array = [1, 56, 4.34, 6, -2]
        const resultado = somarPosicaoPares( array )
        resultado.should.equal(3.34)
      } )

      it( 'soma valores das posições pares do array com valores nulos', function() {
        const array = [0, 0, 0, 0, 0]
        const resultado = somarPosicaoPares( array )
        resultado.should.equal(0)
      } )

      it( 'soma valores das posições pares do array com valores negativos', function() {
        const array = [-1, -1, -1, -1, -1]
        const resultado = somarPosicaoPares( array )
        resultado.should.equal(-3)
      } )

      it( 'soma valores de array vazio', function() {
        const array = []
        const resultado = somarPosicaoPares( array )
        resultado.should.equal(0)
      } )

      it( 'soma valores de array vazio', function() {
        const array = []
        const resultado = somarPosicaoPares( array )
        resultado.should.equal(0)
      } )
    })