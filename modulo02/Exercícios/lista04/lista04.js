function somarPosicaoPares(array){
    var soma = 0
    for(var i = 0; i < array.length; i++){
        if(i % 2 === 0) {
            soma += array[i];
        }
    }
    return soma;
}

function formatarElfos(arrayElfos){
    for(var elfo of arrayElfos){
        elfo.nome = elfo.nome.toUpperCase()
        elfo.temExperiencia = elfo.experiencia > 0 ? true : false
        const textoFlechas = elfo.qtdFlechas !== 1 ? "s": ""
        elfo.descricao = elfo.temExperiencia === true? `${elfo.nome} com experiência e com ${elfo.qtdFlechas} flecha${textoFlechas}` : `${elfo.nome} sem experiência e com ${elfo.qtdFlechas} flecha${textoFlechas}`
        delete elfo.experiencia
        
    }
    return arrayElfos
}