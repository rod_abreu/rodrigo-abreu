function rodarPrograma(){
    
    const $dadosPokemon = document.getElementById('dadosPokemon')
    const $h2 = $dadosPokemon.querySelector('h2')
    
    const $img = $dadosPokemon.querySelector('#thumb')
    const $numeroPokemon = document.getElementById('numeroPokemon')
    
    const $idPokemon = document.getElementById('idPokemon')
    const $altura = document.getElementById('altura')
    
    const $peso = document.getElementById('peso')
    const $btnSorte = document.getElementById('btnSorte')
    

    //estou com sorte!!!
    $btnSorte.onclick = function(){
        var num = Math.floor(Math.random()*802+1)
        
        const $dadosPokemon = document.getElementById('dadosPokemon')
        const $h2 = $dadosPokemon.querySelector('h2')
        const $img = $dadosPokemon.querySelector('#thumb')
        
        let url = `https://pokeapi.co/api/v2/pokemon/${num}/`
        
        fetch( url)
        .then(res => res.json())
        .then(dadosJson => {
            $h2.innerText = dadosJson.name
            $img.src = dadosJson.sprites.front_default
            $idPokemon.innerText = `ID: ${dadosJson.id}`
            $altura.innerText = `Altura: ${parseInt(dadosJson.height)*10}cm`
            $peso.innerText = `Peso: ${(parseInt(dadosJson.weight)/10)}kg`

            //inserindo lista de tipos
            var ul = document.createElement('ul')

            ul.id = 'tipos'
            ul.innerText = "Lista Tipos:"

            $dadosPokemon.appendChild(ul)

            for(var i = 0; i < dadosJson.types.length; i++){

                var li = document.createElement('li')
                li.id = 'seila'

                li.innerText = dadosJson.types[i].type.name
                ul.appendChild(li)
            }

            //inserindo estatísticas
            for(var i = 0; dadosJson.stats.length; i++){

                var ul = document.createElement('ul')
                ul.innerText = dadosJson.stats[i].stat.name

                var li = documento.createElement('li')
                li.innerText = `Base Stat: ${dadosJson.stats[i].base_stat}`

                var li2 = documento.createElement('li')
                li2.innerText = `Effort: ${dadosJson.stats[i].base_effort}`

                ul.appendChild(li)
                ul.appendChild(li2)
                $dadosPokemon.appendChild(ul)
            }
        })
    }
    
    // inserindo normal
    $numeroPokemon.onblur = function(){
        
        let url = `https://pokeapi.co/api/v2/pokemon/${numeroPokemon.value}/` //precisa ser uma string
        let requisicao = fetch(url) //buscar informações da url
        
        //TODO
        //input type text só aceitar numero (OK)
        //quando der um tab dispara a request (OK)
        //ID do pokémon
        //Altura em cm (API retorna em dezenas de cm, ex: 20 é 200cm)(OK)
        //Peso em kilos (API retorna em gramas)(OK)
        //Lista de tipos ()
        //Estatísticas ()
        //Estou com sorte (OK)
        
        
        requisicao
        .then(res => res.json())
        .then(dadosJson => {
            $h2.innerText = dadosJson.name
            $img.src = dadosJson.sprites.front_default
            $idPokemon.innerText = `ID: ${dadosJson.id}`
            $altura.innerText = `Altura: ${parseInt(dadosJson.height)*10}cm`
            $peso.innerText = `Peso: ${(parseInt(dadosJson.weight)/10)}kg`

            //inserindo lista de tipos
            var ul = document.createElement('ul')

            ul.id = 'tipos'
            ul.innerText = "Lista Tipos:"

            $dadosPokemon.appendChild(ul)

            for(var i = 0; i < dadosJson.types.length; i++){

                var li = document.createElement('li')
                li.id = 'seila'

                li.innerText = dadosJson.types[i].type.name
                ul.appendChild(li)
            }

            //inserindo estatísticas
            for(var i = 0; dadosJson.stats.length; i++){

                var ul = document.createElement('ul')
                ul.innerText = dadosJson.stats[i].stat.name

                var li = documento.createElement('li')
                li.innerText = `Base Stat: ${dadosJson.stats[i].base_stat}`

                var li2 = documento.createElement('li')
                li2.innerText = `Effort: ${dadosJson.stats[i].base_effort}`

                ul.appendChild(li)
                ul.appendChild(li2)
                $dadosPokemon.appendChild(ul)
            }
        }
        )
    }
}

rodarPrograma()