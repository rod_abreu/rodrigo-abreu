//exerc.1 (Status: OK)
function calcularCirculo({raio, tipoCalculo}){
    if(raio < 0){
        return 0
    }else{
        if(tipoCalculo   === 'A'){
            return (Math.PI * Math.pow(raio, 2));
        }
        if(tipoCalculo === 'C'){
            return (Math.PI * 2 * raio);
        }
    }
    
}
//exerc.2 (Status: OK)
function naoBissexto(ano){
    if(ano % 400 === 0 || ano % 4 === 0 && ano % 100 !== 0){
        return false;
    }else{
        return true;
    }
}
//exerc.3 -> quando for undefined (Status: OK)
function concatenarSemUndefined ( x = '', y = ''){ //quando se quer pré-definir um valor padrão caso não seja passado por parâmetro
    return x + y;
}

//exerc.4 -> quando for null (Status: OK)
function concatenarSemNull(x, y){
    if(x == null){
        x = "";
    }
    if(y == null){
        y = "";
    }
    return x + y;
}

//exerc.5 -> quando for null || undefined (Status: )
function concatenarEmPaz(x = '',y = ''){
    if(x == null){
        x = "";
    }
    if(y == null){
        y = "";
    }
    return x + y;
}

//exerc.6 -> utiliando currying (Status: OK)
function adicionar(num1) {
    return function adicionar(num2){
            return num1 + num2;
    }
}

//exerc.7 -> soma os n elementos da sequencia de fibonacci (Status: OK)
function fiboSum(num){

    if( num <=0 ){
        return 0
    }else{
        var t1 = 0,
            t2 = 1;

        const n = num
        
        var soma = 1;

        for(var i = 1; i < n; i++){
            var termo = t1 + t2;
            
            soma += termo;

            t1 = t2;
            t2 = termo;
        }
        return soma;
    }
}

function fiboRecursivo(n){
    
    function fibonacci (n){
        if(n === 1 || n === 2){
            return 1
        }
        return fibonacci(n - 1) + fibonacci(n - 2)
    }

    if( n === 1){
        return 1
    }
    return fibonacci(n) + fiboRecursivo(n-1)
}