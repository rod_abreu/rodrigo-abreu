describe( 'concatenarSemUndefined', function() {

    beforeEach( function() {
        chai.should()
      } )

      it( 'concatenar quando for undefined e Soja', function() {
        const x = undefined
        const y = "Soja"
        const resultado = concatenarSemUndefined(x,y)
        resultado.should.equal("Soja")
      } )

      it( 'concatenar quando for os parâmtros -> Soja e é bom', function() {
        const x = "Soja "
        const y = "é bom"
        const resultado = concatenarSemUndefined(x,y)
        resultado.should.equal("Soja é bom")
      } )

      it( 'concatenar apenas 1 parâmetro -> Soja é', function() {
        const x = "Soja é"
        const resultado = concatenarSemUndefined(x)
        resultado.should.equal("Soja é")
      } )

    })