describe( 'somaDiferentona', function() {

    beforeEach( function() {
        chai.should()
      } )

      it( 'adicionar  3+ 4 = 7', function() {
        const x = 3
        const y = 4
        const resultado = adicionar(x)(y)
        resultado.should.equal(7)
      } )

      it('adicionar 5642 + 8749 = 14391', function(){
          const x = 5642
          const y = 8749
          const resultado = adicionar(x)(y)
          resultado.should.equal(14391)
      } )

      it('adicionar (-1) + (-1) = -2', function(){
        const x = -1
        const y = -1
        const resultado = adicionar(x)(y)
        resultado.should.equal(-2)
      } )

     it('adicionar 0 + 0 = 0', function(){
        const x = 0
        const y = 0
        const resultado = adicionar(x)(y)
        resultado.should.equal(0)
    } )
})


