describe( 'concatenarEmPaz', function() {

    beforeEach( function() {
        chai.should()
      } )

      it( 'concatenar parâmetro Soja + undefined', function() {
        const x = undefined
        const y = "Soja"
        const resultado = concatenarEmPaz(x,y)
        resultado.should.equal("Soja")
      } )

      it( 'concatenar parâmetro oja é bom, mas... + null', function() {
        const x = "Soja é bom, mas..."
        const y = null
        const resultado = concatenarEmPaz(x,y)
        resultado.should.equal("Soja é bom, mas...")
      } )

      it( 'concatenar apenas com um parâmetro Soja é', function() {
        const x = "Soja é"
        const resultado = concatenarEmPaz(x)
        resultado.should.equal("Soja é")
      } )
} )

