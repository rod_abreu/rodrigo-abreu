class Jedi{
    constructor(nome){
        this.nome = nome
        this.h1 = document.createElement('h1')
        this.h1.innerText = this.nome
        this.h1.id = `jedi ${ this.nome}`
        const dadosJedi = document.getElementById('dadosJedi')
        dadosJedi.appendChild(this.h1)
    }
    atacarComSelf(){
        //const h1 = documet.getElementById(`jedi ${this.nome}`)
        let self = this
        setTimeout(function(){
            self.h1.innerText += 'atacou'
        },1000)
    }

    atacarComBind(){
        //const h1 = documet.getElementById(`jedi ${this.nome}`)
        setTimeout(function(){
            this.h1.innerText += 'atacou'
        }.bind(this), 1000) //preserva o valor de this
    }

    atacarComBind(){
        //const h1 = documet.getElementById(`jedi ${this.nome}`)
        setTimeout( () => {
            this.h1.innerText += 'atacou!'
        }, 1000) //preserva o valor de this
    }
}