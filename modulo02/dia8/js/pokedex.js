function rodarPrograma(){

    const $dadosPokemon = document.getElementById('dadosPokemon')
    const $h1 = $dadosPokemon.querySelector('h1')
    const $img = $dadosPokemon.querySelector('#thumb')
    const $numeroPokemon = $dadosPokemon.querySelector('#numeroPokemon')
    const $idPokemon = document.getElementById('idPokemon')
    const $altura = document.getElementById('altura')
    const $peso = document.getElementById('peso')
    const $tipoPokemon = document.getElementById('tipoPokemon')

    $numeroPokemon.onblur = function(){
    
    let url = `https://pokeapi.co/api/v2/pokemon/${numeroPokemon.value}/` //precisa ser uma string
    let requisicao = fetch( url) //buscar informações da url
    
    //TODO
    //input type text só aceitar numero (OK)
    //quando der um tab dispara a request (OK)
    //ID do pokémon
    //Altura em cm (API retorna em dezenas de cm, ex: 20 é 200cm)(OK)
    //Peso em kilos (API retorna em gramas)(OK)
    //Lista de tipos (é dinâmica, pode ter mais de um). Ex: Weepinbell é dos tipos "poison" e "grass". Lembre-se dos elementos HTML para listas
    //Estatísticas (são dinâmicas e percentuais). Não é preciso traduzir.

    
    requisicao
        .then(res => res.json())
        .then(dadosJson => {
            $h1.innerText = dadosJson.name
            $img.src = dadosJson.sprites.front_default
            $idPokemon.innerText = `id: ${dadosJson.id}`
            $altura.innerText = `Altura: ${dadosJson.height}cm`
            $peso.innerText = `Peso: ${dadosJson.weight}kg`
            $tipoPokemon.innerText = `Tipos: `
        }
        )
    }
}

rodarPrograma()