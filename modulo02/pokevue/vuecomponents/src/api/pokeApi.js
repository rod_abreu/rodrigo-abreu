import Pokemon from '../models/pokemon.js'

export default class PokeApi {
  constructor( url ) {
    this.url = 'https://pokeapi.co/api/v2/'
  }
  
  async buscar( params ) {
    let urlPokemon = params.urlPokemonPreenchida ||`${ this.url }/${ params.idPokemon }/`
    
    return new Promise (resolve => {
      fetch( urlPokemon )
      .then( j => j.json() )
      .then( p => {
        const pokemon = new Pokemon( p )
        // return "assícrono",
        // vai jogar o valor de pokemon para quem chamou utilizá-lo de forma assíncrona
        resolve( pokemon )
      } )
    })
  }
  
  async listarPorTipo( tipoPokemon, qtdPokemons, pagina) {
    let urlPokemon = `${ this.url }/type/${ tipoPokemon }/`
    
    const fim =  qtdPokemons * pagina
    const inicio = fim - qtdPokemons
    
    return new Promise( resolve => {
      fetch( urlPokemon )
      .then( j => j.json() )
      .then( p => {
        const pokemons = p.pokemon.slice( inicio, fim )
        const promisePokemons = pokemons.map( p => this.buscar( { urlPokemonPreenchida: p.pokemon.url }))
        Promise.all(promisePokemons).then( resultadoFinal => {
          resolve(resultadoFinal)
        })
      } )
    } ) 
  }
}