import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Login from './components/screens/Login.vue'
import Home from './components/screens/Home.vue'
import VeeValidate, { Validator } from 'vee-validate' 
import ptBR from 'vee-validate/dist/locale/pt_BR' //importando a tradução Português Brasil

Validator.localize('pt_BR', ptBR); //adicionando o idioma português para o erros do código


Vue.use(VeeValidate);
Vue.use(VueRouter)
Vue.config.productionTip = false

//criando um objeto(array) para posição de pagina
const routes = [
  {path: '/', component: Login},
  { name:'home', path:'/home/:usuario', component:Home }
]

const router = new VueRouter({
  routes //quando o nome do campo seja routes e o valor do campo seja routes, não precisa escrever routes: routes, aceita apenas routes
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
