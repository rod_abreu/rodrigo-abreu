export default class Pokemon {
  constructor( jsonVindoDaApi ) {
    this.nome = jsonVindoDaApi.name
    this.id = jsonVindoDaApi.id
    this.img = jsonVindoDaApi.sprites.front_default
    this._altura = jsonVindoDaApi.height
    this._peso = jsonVindoDaApi.weight
    this.tipos = jsonVindoDaApi.types.map( t => t.type.name )

    // TODO: pegar outros campos do JSON
    
    this.estatisticas = jsonVindoDaApi.stats.map(t => t.stat.name + " " + t.base_stat)
  }

  // como a altura vem em dezenas de cm, estamos aplicando um cálculo para traduzir para cm
  get altura() {
    return this._altura * 10
  }
  get peso(){
    return parseInt(this._peso)/10
  }
}