console.log("Começou!")

//comentário
/*
* Outro tipo de comentário
*/
//elgo está no escopo global

var elfo = {
    nome: "Legolas",
    experiencia: 0,
    qntFlechas: 7,
    atirarFlecha: function(nome){
        this.qntFlechas-- //this vai guardar nele o contexto de execução de uma função
        this.experiencia++
        return nome + "!!!"
    }
}   
