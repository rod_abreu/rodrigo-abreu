const urlApi = 'https://pokeapi.co/api/v2/pokemon'
const pokeApi = new PokeApi( urlApi )

let app = new Vue( {
    el: '#meuPrimeiroApp',
    data: {
        idParaBuscar: '',
        pokemon: { },
        idSorte: ''
    },
    methods: {
        async buscar() {
            this.pokemon = await pokeApi.buscar( this.idParaBuscar )
        },
        async estouComSorte(){
            
            var idSorte = Math.floor(Math.random()*802+1)
            this.idParaBuscar = idSorte
            this.pokemon = await pokeApi.buscar(this.idParaBuscar)
        }
    }
} )
