const meuH2 = document.getElementById('tituloPagina')

function perguntarNome() {
    const nome = prompt('Qual o seu nome?')
    meuH2.innerText = nome
    localStorage.nome = nome
}

function renderizaNomeArmazenadoNaTela() {
    meuH2.innerText = localStorage.nome
}

function rodarPrograma() {
const nomeArmazenado = localStorage.nome && localStorage.nome.length > 0
if (nomeArmazenado) {
    renderizaNomeArmazenadoNaTela()
} else {
    perguntarNome()
}

//criando um contador no localStorage
if(!localStorage.count){
    localStorage.count = 0
}

const btnLimparDados = document.getElementById('btnLimparDados')
const temQueDesabilitar = localStorage.count >= 5
if(temQueDesabilitar){
    btnLimparDados.disabled = true
}

btnLimparDados.onclick = function() {
    let armazenamentoLocal = localStorage
    armazenamentoLocal.removeItem( 'nome' )
    localStorage.count++
    if ( localStorage.count >= 5 ) {
      btnLimparDados.disabled = true
    }
  }
}

rodarPrograma()