# #VEMSERDBC

## Programa de capacitação Vem ser DBC by DBC Company

### modulo01 - Orientação Objeto (OO);

Instrutor: Bernardo Bosak

Metodologia: Criação do jogo As Duas Torres de Chopp.

Aplicação de conceitos de OO utilizando o BlueJ;

- Interfaces;
- Herança;
- Polimorfismo;
- Classes Abstratas;
- ArrayList, List;

### módulo02 - HTML, CSS e JavaScript;

Instrutor: Bernardo Bosak

Aplicação de conceitos sobre HTML, CSS e JavaScript;

- Criação de um HTML sobre uma cidade (Springfield);
- Reprodução de bandeiras utilizando Flexbox/Grid;
- Conceitos de Script (Declaração de variáveis, functions);
- Criação do projeto PokeDex;
- Introdução ao Vue.JS;
- Recriação do projeto PokeDex utilizando Vue.js;

### módulo03 - JAVA Back End

Instrutor: Tiago Rodrigues da Rosa

Aplicação de conceitos JAVA

- Diagrama ER, Sql Developer;
- MVC, EJB, JAVA EE;
- JPA, Hibernate;
- Projeto Petshop;
- Projeto Locadora;